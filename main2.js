var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

data = {};

var counter = 6;

function addElement(classtype,inputdata){
  var div1  = document.createElement("div")
  div1.className = classtype;
  div1.innerHTML = "<span class='myspan'>"+inputdata+"</span>";
  document.getElementById("main").appendChild(div1);
}

function load_rows(k){
  var no_of_rows = data.data.rows.length;
  var i=k;
  while( i < k+6){
    if(!(i%2==0)){
      // set ratio as 2:1:1 - odd
      if(data.data.rows[i])
        addElement("div2",data.data.rows[i].author_name);
      if(data.data.rows[i+1])
        addElement("div1",data.data.rows[i+1].author_name)
      if(data.data.rows[i+2])
        addElement("div1",data.data.rows[i+2].author_name)
      i +=3;
    }
    else{
      // set ratio as 1:1:2 - even
      if(data.data.rows[i])
        addElement("div1",data.data.rows[i].author_name);
      if(data.data.rows[i+1])
        addElement("div1",data.data.rows[i+1].author_name)
      if(data.data.rows[i+2])
        addElement("div2",data.data.rows[i+2].author_name)
      i+=3;
    }
  }
  return i;

}

function load_more(){
  if(counter < data.data.rows.length){
    counter = load_rows(counter);
  }
  else{
    // change the name of Button
    console.log("Complete");
    var btn = document.getElementById("btn1");
    btn.innerText = "No More Records!";

  }
}

getJSON('https://s3-ap-southeast-1.amazonaws.com/takehomeproject/feed.json',
function(err, datas) {
  if (err !== null) {
    alert('Something went wrong: ' + err);
  } else {
      //console.log(data);
     //window.data = datas;
      data = datas;
      load_rows(0);
  }
});


