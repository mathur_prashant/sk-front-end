var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};
function addElement(classtype,inputdata){
      var div1  = document.createElement("div")
      div1.className = classtype;
      div1.innerHTML = "<span class='myspan'>"+inputdata+"</span>";
      document.getElementById("main").appendChild(div1);
}

getJSON('https://s3-ap-southeast-1.amazonaws.com/takehomeproject/feed.json',
function(err, data) {
  if (err !== null) {
    alert('Something went wrong: ' + err);
  } else {
      //console.log(data);
      var no_of_rows = data.data.rows.length;
      var i= 0 ;
      while( i < (no_of_rows)){
        if(!(i%2==0)){
          // set ratio as 2:1:1 - odd
          if(data.data.rows[i])
            addElement("div2",data.data.rows[i].author_name);
          if(data.data.rows[i+1])
            addElement("div1",data.data.rows[i+1].author_name)
          if(data.data.rows[i+2])
            addElement("div1",data.data.rows[i+2].author_name)
          i +=3;
        }
        else{
          // set ratio as 1:1:2 - even
          if(data.data.rows[i])
            addElement("div1",data.data.rows[i].author_name);
          if(data.data.rows[i+1])
            addElement("div1",data.data.rows[i+1].author_name)
          if(data.data.rows[i+2])
            addElement("div2",data.data.rows[i+2].author_name)
          i+=3;
        }
      }
  }
});